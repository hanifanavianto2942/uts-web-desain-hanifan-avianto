## Cara Run Program

- Install XAMPP terlebih dahulu [Install Xampp](https://sourceforge.net/projects/xampp/files/XAMPP%20Windows/8.2.4/xampp-windows-x64-8.2.4-0-VS16-installer.exe).
- Setelah itu clone project dengan cara **git clone https://gitlab.com/hanifanavianto2942/uts-web-desain-hanifan-avianto.git** didalam folder **htdocs** atau download **ZIP** dan extract di dalam folder **htdocs**.
- Ubah isi file **index.php** di dalam folder **htdocs**, ubah link nya saja menjadi folder yang sudah di extract tadi.
- Jika sudah jalankan program dengan cara menyalakan apache terlebih dahulu di dalam **xampp control panel**, untuk mencari xampp control panel bisa dilakukan di pencarian windows.
- Jika sudah di nyalakan apache, lalu buka browser dan ketikan di bagian url **http://127.0.0.1** / **http://localhost**
- Setelah itu anda dapat menggunakan fitur kirim email yang dapat anda terima melalui email anda.