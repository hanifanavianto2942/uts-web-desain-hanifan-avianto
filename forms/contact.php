<?php
if (isset($_POST['name']) || isset($_POST['email'])) {
  $name = $_POST['name'];
  $email = $_POST['email'];
  $subject = $_POST['subject'];
  $message = $_POST['message'];

  $ch = curl_init();
  $url = 'https://hanifan.pesanin.com/api/contact/form';
  $data = [
    "name" => $name,
    "email" => $email,
    "subject" => $subject,
    "message" => $message
  ];

  $data = json_encode($data);
  $headers = array(
    "Content-Type: application/json",
    "Access-Control-Allow-Origin: *"
  );
  curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
  curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
  $server_output = curl_exec($ch);
  curl_close($ch);
  $get = json_decode($server_output, true);

  if ($get['status'] == true) :
    echo "OK";
  else :
    echo $get['msg'];
  endif;
}
